# 1)
# Чтение текста из файла, где каждый элемент - отдельный пользователь
file = open('file3.txt').read().split('\n')

d = dict() # Создание словаря

for item in file:
    user = item.split(' ') # Разбиение пользователя на пароль и логин
    d[user[0]] = user[1] # Запись в словарь (имя - ключ, пароль - значение)

print("Введите имя: ")
name = input()
print("Введите пароль: ")
password = input()

if name in d.keys(): # Если такое имя пользователя уже есть
    print(d[name])
else: # иначе создаём новый элемент в словаре
    d[name] = password
    print(d) # Вывод словаря для проверки изменений

# 2)
# Чтение текста из файла, где каждый элемент - отдельный пользователь
file = open('file3.txt').read().split('\n')

d = dict() # Создание словаря

for item in file:
    user = item.split(' ') # Разбиение пользователя на пароль и логин
    d[user[0]] = user[1] # Запись в словарь (имя - ключ, пароль - значение)

keys = list(d.keys()) # Создание списка из ключей (имен)

for i in range(len(keys) - 1): # Сортировка ключей по алфавиту
    for j in range(len(keys) - i - 1):
        if keys[j][0] > keys[j+1][0]:
            keys[j], keys[j+1] = keys[j+1], keys[j]

for key in keys: # Вывод ключей (имён)
    print(key)

# 3)
# Чтение текста из файла, где каждый элемент - отдельный пользователь
file = open('file3.txt').read().split('\n')

d = dict() # Создание словаря

for item in file:
    user = item.split(' ') # Разбиение пользователя на пароль и логин
    d[user[0]] = user[1] # Запись в словарь (имя - ключ, пароль - значение)

val = list(d.values()) # Создание списка из значений (паролей)

for i in range(len(val) - 1): # Сортировка паролей по длине
    for j in range(len(val) - i - 1):
        if len(val[j]) > len(val[j+1]):
            val[j], val[j+1] = val[j+1], val[j]

for v in val: # Вывод значений (паролей)
    print(v)

# 4)
# разбиение текста на слова
text = "To explore different supervised learning algorithms, we're going to use aп algorithms of small synthetic or artificial datasets as examples, together with some larger real world datasets".replace(",", "").split(" ")
d = dict()

for item in text:
    d[item] = item.upper()

for key, value in d.items():
    print(key + " : " + value)

# 5)
    # разбиение текста на слова
    text = "To explore different supervised learning algorithms, we're going to use aп algorithms of small synthetic or artificial datasets as examples, together with some larger real world datasets".replace(
        ",", "").split(" ")
    d = dict()

    for item in text:
        d[item] = item.upper()

    val = list(d.values())

    for i in range(len(val) - 1):  # Сортировка значение по уменьшению длины
        for j in range(len(val) - i - 1):
            if len(val[j]) < len(val[j + 1]):
                val[j], val[j + 1] = val[j + 1], val[j]

    for value in val:
        print(value)
