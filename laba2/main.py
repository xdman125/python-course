# 1)
def lastNumber (x):
    return x % 10

print(lastNumber(123))
print(lastNumber(432809))

# 2)
def cylinder_vol(radius, height):
    return (radius * radius * 3.14) * height

print(cylinder_vol(5,4))
print(cylinder_vol(3.5,0.7))

# 3)
def cylinder_vol(radius, height):
    return (radius * radius * 3.14) * height

print(cylinder_vol(5,4)) # позиционные аргументы
print(cylinder_vol(height = 3, radius = 2)) # использование имён параметров

# 4)
def cylinder_vol(radius, height = 10):
    return (radius * radius * 3.14) * height

print(cylinder_vol(5)) # используя значения параметров по умолчанию
print(cylinder_vol(1, 3)) # не используя
# 5)
areaCircle = lambda r: r * r * 3.14

def cylinder_vol(radius, height, fn):
    return fn(radius) * height

print(cylinder_vol(2, 3, areaCircle))
print(cylinder_vol(3, 1.5, areaCircle))
print(cylinder_vol(1, 1, areaCircle))
