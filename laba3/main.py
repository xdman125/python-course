# 1)
def arithmetic(numb1, numb2, operation):
    # преобразование в тип int
    numb1 = int(numb1)
    numb2 = int(numb2)

    if operation == '+':
        return numb1 + numb2
    elif operation == '-':
        return numb1 - numb2
    elif operation == '*':
        return numb1 * numb2
    elif operation == '/':
        return numb1 / numb2
    else:
        return 'Неизвестная операция'


print('Введите первое число: ')
n1 = input()
print('Введите второе число: ')
n2 = input()
print('Введите операцию(+, -, *, /): ')
oper = input()

print(arithmetic(n1, n2, oper))

# 2)
password = ""
print('Введите имя: ')
name = input()

while password != "No":
    print('Введите пароль: ')
    password = input()

    if password == "1111_p":
        print(name+" - Ваш пароль правильный")
        break
    elif password == "No":
        break
    else:
        print("Пароль неверный, повторите ввод")

print("Done")
