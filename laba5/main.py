# 1)
# Преобразование строки в массив слов
text = "In February 2021 Gorman was highlighted in Time magazine 100 Next List under the category of Phenoms".split(" ")
textLowCase = [] # массив для хранения элементов нижнего регистра
textUpperCase = [] # массив для хранения элементов верхнего регистра

# Проход по всем словам предложения
for word in text:
    if word[0] < '0' or word[0] > '9': # отсеивание чисел
        if word[0] >= 'a' and word[0] <= 'z' : # отсеивание слов с заглавной буквой
            textLowCase.append(word)
        elif word[0] >= 'A' and word[0] <= 'Z': # отсеивание нижнего регистра
            textUpperCase.append(word)

for i in range(len(textLowCase) - 1): # сортировка массива по алфавиту
    for j in range(len(textLowCase) - i - 1):
        if textLowCase[j][0] > textLowCase[j+1][0]:
            textLowCase[j], textLowCase[j+1] = textLowCase[j+1], textLowCase[j]

for i in range(len(textUpperCase) - 1): # сортировка массива в обратном алфавитном порядке
    for j in range(len(textUpperCase) - i - 1):
        if textUpperCase[j][0] < textUpperCase[j+1][0]:
            textUpperCase[j], textUpperCase[j+1] = textUpperCase[j+1], textUpperCase[j]

# Преобразование массивов в строки
textLowCase = ' '.join(textLowCase)
textUpperCase = ' '.join(textUpperCase)

tp = (textLowCase, textUpperCase) # Создание кортежа

print(tp[0] + "," + tp[1]) # Вывод кортежа

# 2)
file = open('file1.txt', 'r') # Открытие файла на чтение
# чтение содержимого файла
# разбиение в массив по словам
text = file.read().replace('\n', ' ').split(" ")
# так в последний элемент массива записывается пуста строка, то последний элемент можно удалить
text.pop()

# Удаление всех чисел из массива
for item in text:
    if item[0] >= '0' and item[0] <= '9':
        text.remove(item)

# сортировка массива по алфавиту
for i in range(len(text) - 1):
    for j in range(len(text) - i - 1):
        if text[j][0] > text[j+1][0]:
            text[j], text[j+1] = text[j+1], text[j]

file.close() # Закрытие файла

file = open('file1.txt', 'a') # Открытие файла на дозапись

file.write("\n") # Запись пустой строки

for item in text: # запись слов
    file.write(item + " ")
