# 1)
# старый стиль форматирования
def circumference(radius):
    print("Длина окружности с радиусом %7g равна %g" % (radius, 2 * 3.14 * radius))

circumference(5)
# новый стиль форматирования
def circumference(radius):
    print("Длина окружности с радиусом       {} равна {}".format(radius, round(2 * 3.14 * radius, 4)))

circumference(6)

# 2)
print("Радиус в восьмеричном представлении {0:o} и в шестнадцатеричном представлении {0:x} , \n в экспоненциальном представлении {0:E}, в десятичном представлении {0:d}".format(308))

# 3)
file1 = open('file1.txt', 'w')
text = ["Бананы 12.23", "Ананасы 3.40", "Кокосы 124.23", "Яблоки 233.40", "Лимоны 789.23", "Гранат 55.71", "Виноград 5.20", "Сливы 233.40"]

for i in range(len(text)):
    file1.write(text[i])
    if i % 2 == 1:
        file1.write("\n")
    else:
        file1.write(" ")
file1.close()

# 4)
file1 = open('file1.txt')
file2 = open('file2.txt', 'w')

for line in file1:
    if line[-2] == '0':
        file2.write(line)
file1.close()
file2.close()

# 5)
file1 = open('file1.txt')
counter = 0

for line in file1:
    if line[-2] == '0':
        counter += 1

print(counter)
file1.close()

# 6)
print("Введите название файла: ")
nameFile = input()

if nameFile == "file1.txt" or nameFile == "file2.txt":
    file = open(nameFile)
    text = file.read().splitlines()
    for line in text:
        print(line)
        file.close()
else:
    print("Неверное имя файла!")
